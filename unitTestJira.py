import xml.etree.ElementTree as ET
import enum
import urllib3
import requests
import json


from jira import JIRA
from jira.client import ResultList
from jira.resources import Issue
issueKey = 'NULL'

#SETTINGS FOR JIRA
url="https://ajeshz.atlassian.net//rest/api/2/issue"
headers={
    "Accept": "application/json",
    "Content-Type": "application/json"
}


options = {'server': 'https://ajeshz.atlassian.net'}
jira = JIRA(basic_auth=("ajeshz111@gmail.com", "Qvv4Q9xEFgSuYt2RWz7821B5"),options = {'server': 'https://ajeshz.atlassian.net'})
# Get the mutable application properties for this server (requires
# jira-system-administrators permission)
props = jira.application_properties()


tree = ET.parse('test_results.xml')
root = tree.getroot()

for test in root.iter('test'):
    rootString = root.attrib
    arribString = test.attrib
    #arribString = test.find('error')
    jiraSummary = rootString.get('name')  + '  :  ' + arribString.get('name')
    print jiraSummary
    
    if((test.find('error')) is not None):

        jqlString = 'summary ~\"'+ jiraSummary +'\" AND status =\"To Do\" OR status =\"In Progress\"'
        ResultList = jira.search_issues(jql_str= jqlString, json_result=True, fields="summary")

        for i in ResultList['issues']:
            kVal = i.values()
            issueKey = kVal[0]

        if(issueKey is not 'NULL'):
            print issueKey
        else:
            print "CREATE A TICKET FOR THIS"

        
    	    #payload generation
    	    payload=json.dumps(
           	{
		"fields": {
        	   "project":
	           {
        	      "key": "TES"
	           },
        	   "summary": jiraSummary,
	           "description": 'Ticket from Unit Test Module',
        	   "issuetype": {
	              "name": "Task"
        	   }
	       }
	    }
	    )
            #create the jira ticket here
	    response=requests.post(url,headers=headers,data=payload,auth=("ajeshz111@gmail.com","Qvv4Q9xEFgSuYt2RWz7821B5"))
	    data=response.json()
    	    print(data["id"])
    	    print '-------------------------------------'
    
    else:
        jqlString = 'summary ~\"'+ jiraSummary +'\" AND status =\"To Do\" OR status =\"In Progress\"'

        ResultList = jira.search_issues(jql_str= jqlString, json_result=True, fields="summary")

        for i in ResultList['issues']:
            kVal = i.values()
            issueKey = kVal[0]
            print issueKey
            if(issueKey is not 'NULL'):    #close the ticket 
                urlClose ='https://ajeshz.atlassian.net//rest/api/3/issue/'+ issueKey +'/transitions'
                payload2=json.dumps(
                        {
                            "transition": {
                            "id": "31"
                            } 
                        }
                        )
                response=requests.post(urlClose,headers=headers,data=payload2,auth=("ajeshz111@gmail.com","Qvv4Q9xEFgSuYt2RWz7821B5"))
                print 'Issue '+ issueKey+ ' is Closed'


