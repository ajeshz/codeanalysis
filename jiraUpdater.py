
import enum
import urllib3
import requests
import json

#SETTINGS FOR JIRA 
url="https://ajeshz.atlassian.net//rest/api/2/issue"
headers={
    "Accept": "application/json",
    "Content-Type": "application/json"
}


issueStatus=0   
creationDate=1
issueHash=2
severity=3      #USED 
author=4
issueType=5     #USED BUG/WARN/CODESMELL
message=6
tags=7
component=8     
rule=9
project=10
flows=11
key=12
scope=13
line=14
debt=15
effort=16
textRange=17
updateDate=18


http = urllib3.PoolManager()
myHeaders = urllib3.util.make_headers(basic_auth='admin:test123')
info_t = "INFO"
blocker_t = "BLOCKER"
critical_t = "CRITICAL"
minor_t = "MINOR"
major_t = "MAJOR"
json_url_project = "IssueTest_v0.1"
json_url_string_part = 'http://129.159.62.193:8050/api/issues/search?componentKeys='+json_url_project+'&resolved=false&severities='
json_url_level = critical_t
json_url_string = json_url_string_part + json_url_level
#json_url = http.request('GET', 'http://129.159.62.193:8050/api/issues/search?componentKeys=nRF_ruleCheck&resolved=false&severities=INFO', headers=myHeaders)
json_url = http.request('GET', json_url_string, headers=myHeaders)

data = json_url.data.decode('utf-8')
values = json.loads(data)

print 'ISSUE COUNT', values['total']
print '\n-------------------------------------'

print 'ISSUE TYPE', json_url_level 
print '\n-------------------------------------'


for i in values['issues']:
    val = i.values()
    print 'File      :',val[component],'\nMessage   :', val[message],\
            '\nIssueType :',val[issueType]
    #create the strings
    jiraSummary = val[issueType]+':'+val[component]
    print jiraSummary
    #2 -- description
    jiraDescription = val[message]
    print jiraDescription

    
    #payload generation
    payload=json.dumps(
        {
        "fields": {
           "project":
           {
              "key": "TES"
           },
           "summary": jiraSummary,
           "description": jiraDescription,
           "issuetype": {
              "name": "Task"
           }
       }
    }
    )
    #create the jira ticket here
    response=requests.post(url,headers=headers,data=payload,auth=("ajeshz111@gmail.com","Qvv4Q9xEFgSuYt2RWz7821B5"))
    data=response.json()
    print(data["id"])
    print '\n\n-------------------------------------'

